<?php
declare(strict_types = 1);

namespace Insidesuki\ApiClient\Authentification;

use Insidesuki\ApiClient\Authentification\Contracts\ApiBearerCredentialInterface;

class BearerCredential implements ApiBearerCredentialInterface
{

	private $apiName;
	private $baseUrl;
	private $authUrl;
	private $bodyAuth;
	private $responseAuth;
	private $expiresAt = 60; // seconds

	public function getApiName(): string
	{
		return $this->apiName;
	}


	public function getBaseUrl(): string
	{
		return $this->baseUrl;
	}


	public function getAuthUrl(): string
	{
		return $this->authUrl;
	}


	public function getBodyAuth(): array
	{
		return $this->bodyAuth;
	}


	public function getResponseAuth(): string
	{
		return $this->responseAuth;
	}


	public function getExpiresAt(): int
	{
		return $this->expiresAt;
	}
}