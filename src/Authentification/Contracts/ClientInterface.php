<?php

namespace Insidesuki\ApiClient\Authentification\Contracts;

use Symfony\Contracts\HttpClient\HttpClientInterface;

interface ClientInterface
{

	public function authenticate();

	public function refresh();

	public function getBaseUrl(): string;

	public function getHttpClient(): HttpClientInterface;

	public function getCredencial(): ApiBearerCredentialInterface;


}