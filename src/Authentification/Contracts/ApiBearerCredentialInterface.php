<?php

namespace Insidesuki\ApiClient\Authentification\Contracts;

interface ApiBearerCredentialInterface
{


	public function getApiName():string;


	public function getBaseUrl():string;


	public function getAuthUrl():string;


	public function getBodyAuth(): array;


	public function getResponseAuth(): string;


	public function getExpiresAt(): int;

}