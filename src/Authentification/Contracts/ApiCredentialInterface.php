<?php

namespace Insidesuki\ApiClient\Authentification\Contracts;

interface ApiCredentialInterface
{

	public function setApiName(string $apiName):void;

	public function getApiName():string;

	public function setBaseUrl(string $url):void;

	public function getBaseUrl():string;




}