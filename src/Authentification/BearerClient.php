<?php
declare(strict_types = 1);

namespace Insidesuki\ApiClient\Authentification;

use Insidesuki\ApiClient\Authentification\Contracts\ApiBearerCredentialInterface;
use Insidesuki\ApiClient\Authentification\Contracts\ClientInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BearerClient implements ClientInterface
{

	public const METTHOD_AUTH = 'POST';


	protected $tokenName;

	protected $cache;

	protected $content;
	/**
	 * @var ApiBearerCredentialInterface
	 */
	protected $credential;
	/**
	 * @var bool
	 */
	protected $storeInCache;
	protected $httpClient;

	public function __construct(HttpClientInterface $httpClient, ApiBearerCredentialInterface $credential, bool $storeInCache = true)
	{
		$this->httpClient   = $httpClient;
		$this->credential   = $credential;
		$this->storeInCache = $storeInCache;
		$this->cache        = new FilesystemAdapter();
		$this->tokenName    = 'api_name.' . $this->credential->getApiName();

	}

	public function storeInCache(bool $cache): void
	{
		$this->storeInCache = $cache;
	}


	/**
	 * @throws InvalidArgumentException
	 */
	public function refresh(bool $auth = true)
	{

		$this->cache->delete($this->tokenName);
		return ($auth) ? $this->authenticate() : true;

	}

	public function authenticate()
	{

		if($this->storeInCache) {
			return $this->checkCached();
		}
		{
			return $this->doAuth();
		}

	}

	private function checkCached()
	{

		// check if token was cached
		$cachedToken = $this->getCachedToken();
		if(null !== $cachedToken) {
			return $cachedToken;
		} else {
			return $this->doAuth();
		}

	}

	/**
	 * Get token value stored in cache system
	 * @throws InvalidArgumentException
	 */
	private function getCachedToken()
	{
		return $this->cache->getItem($this->tokenName)->get();

	}

	/**
	 * @return mixed
	 * @throws InvalidArgumentException
	 * @throws ClientExceptionInterface
	 * @throws DecodingExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws TransportExceptionInterface
	 */
	public function doAuth()
	{
		$data = $this->credential->getBodyAuth();
		$response      = $this->httpClient->request(self::METTHOD_AUTH, $this->credential->getAuthUrl(), $data);
		$this->content = $response->toArray();
		// save to cache
		$responseAuth = $this->content[$this->credential->getResponseAuth()];
		$this->saveTokenToCache($responseAuth);
		return $responseAuth;
	}

	/**
	 * Store token in cache system
	 * @param string $value
	 * @return void
	 * @throws InvalidArgumentException
	 */
	private function saveTokenToCache(string $value)
	{
		if($this->storeInCache) {
			$cachedToken = $this->cache->getItem($this->tokenName);
			$cachedToken->set($value);
			$cachedToken->expiresAfter($this->credential->getExpiresAt());
			$this->cache->save($cachedToken);
		}

	}

	public function getContent(): array
	{

		return $this->content;
	}

	public function getCredencial(): ApiBearerCredentialInterface
	{
		return $this->credential;
	}

	public function getBaseUrl(): string
	{
		return $this->credential->getBaseUrl();
	}

	public function getHttpClient(): HttpClientInterface
	{
		return $this->httpClient;
	}
}