<?php
declare(strict_types = 1);

namespace Insidesuki\ApiClient;

use Insidesuki\ApiClient\Authentification\Contracts\ClientInterface;
use Insidesuki\ApiClient\Exception\ApiException;

/**
 *
 */
class ApiManager
{

	/**
	 * @var ClientInterface
	 */
	protected $client;
	private   $httpClient;
	private   $response;
	private   $content;
	private   $contentType = 'application/json';
	private   $statusCode;
	private   $successCode = 200;

	public const SUCCESS_CODES = [200, 201,404];

	public function __construct(ClientInterface $client)
	{
		$this->client     = $client;
		$this->httpClient = $this->client->getHttpClient();
	}


	public function find(string $url)
	{
		return $this->runRequest('GET', $url);
	}


	protected function runRequest(string $method, string $url, int $successCode = 200)
	{
		$urlPetition       = $this->client->getBaseUrl() . '/' . $url;
		$this->successCode = $successCode;
		$this->response = $this->httpClient->request($method, $urlPetition, [
				'auth_bearer' => $this->client->authenticate()
			]);
		$this->statusCode = $this->response->getStatusCode();
		$this->prepareContentResponse();
		return $this->content;

	}


	private function prepareContentResponse(bool $assoc = true): void
	{

		// 404 is success,the cliente must evaluate this

		if(!in_array($this->statusCode, self::SUCCESS_CODES, true)) {
			throw new ApiException($this->statusCode);
		}
		$this->content         = json_decode($this->response->getContent(false), $assoc);
		$this->content['code'] = $this->statusCode;
	}

	public function store(string $url, array $data)
	{

		return $this->sendData('POST', $url, $data);
	}


	public function put(string $url,array $data){

		return $this->sendData('PUT',$url,$data);
	}

	/**
	 * @param string $method
	 * @param string $url
	 * @param array $data
	 * @param $successCode
	 * @return mixed
	 * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
	 */
	protected function sendData(string $method, string $url, array $data, $successCode = 200)
	{
		$this->successCode = $successCode;
		$dataToSend        = $this->getDataByContentType($data);
		$urlPetition = $this->client->getBaseUrl() . '/' . $url;
		$body = [
			'auth_bearer' => $this->client->authenticate(), 'headers' => [
				'Content-Type' => $this->contentType
			], 'body'     => $dataToSend
		];
		$this->response = $this->httpClient->request($method, $urlPetition, $body);
		$this->statusCode = $this->response->getStatusCode();
		$this->prepareContentResponse();
		return $this->content;

	}

	private function getDataByContentType($data)
	{

		$dataContent = '';
		switch ($this > $this->contentType) {
			case 'application/json';
				$dataContent = json_encode($data);
				break;
			case 'application/x-www-form-urlencoded';
				$dataContent = urlencode($data);
				break;
		}
		return $dataContent;
	}

	protected function changeContentType(string $contentType): void
	{
		$this->contentType = $contentType;
	}

}