<?php
declare(strict_types = 1);

namespace Insidesuki\ApiClient\Exception;

use RuntimeException;

/**
 *
 */
class ApiException extends RuntimeException
{

	public function __construct($message = "")
	{
		parent::__construct(sprintf('Api Error:%s',$message));
	}

}